#!/usr/bin/env php
<?php

require_once 'vendor/autoload.php';

use Import360\Commands\EpisodesImportCommand;
use Import360\Commands\HostsImportCommand;
use Import360\Commands\ImportCommand;
use Import360\Commands\NewsImportCommand;
use Import360\Commands\OperatorsImportCommand;
use Import360\Commands\PollsImportCommand;
use Import360\Commands\ShowsHostsRelationsImportCommand;
use Import360\Commands\ShowsImportCommand;
use Import360\Commands\TagsImportCommand;
use Import360\Commands\WipeDatabaseCommand;
use Symfony\Component\Console\Application;

define('APP_PATH', __DIR__);

$app = new Application('360tv DB Import');
$app->add(new ImportCommand());
$app->add(new WipeDatabaseCommand());
$app->add(new OperatorsImportCommand());
$app->add(new PollsImportCommand());
$app->add(new TagsImportCommand());
$app->add(new ShowsImportCommand());
$app->add(new HostsImportCommand());
$app->add(new ShowsHostsRelationsImportCommand());
$app->add(new NewsImportCommand());
$app->add(new EpisodesImportCommand());
$app->setDefaultCommand('import');
$app->run();
