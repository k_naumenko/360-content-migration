<?php

namespace Import360\Commands;

use HTMLPurifier;
use HTMLPurifier_Config;
use Import360\Db\Connection;
use Import360\Log\Logger;
use Ramsey\Uuid\Uuid;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class HostsImportCommand extends Command
{
    protected function configure()
    {
        $this->setName('import:hosts')
             ->setDescription('Start hosts import')
             ->addOption('fetch_limit', null, InputOption::VALUE_OPTIONAL, 'How many objects program will fetch from DB', 1000);
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $logger = (new Logger('hosts'))->getLogger();
        $logger->info('Starting hosts import', array('content' => 'hosts'));
        $output->writeln(PHP_EOL . '<info>Starting hosts import</info>');

        $getHostsSQL = "SELECT host.name, host.description AS text,
                          (SELECT TOP 1 i.url
                            FROM tbl_images AS i
                            JOIN tbl_images_links il ON il.id_image = i.id
                            WHERE il.obj_id = host.id AND il.main_image = 1 AND il.obj_type = 'Host')
                          AS photo
                        FROM [dbo].[tbl_hosts] AS HOST ORDER BY HOST.id";
        $hosts       = Connection::getSourceDB()->query($getHostsSQL)->fetchAll(\PDO::FETCH_ASSOC);

        $progress = new ProgressBar($output, count($hosts));
        $progress->setMessage('Importing hosts');
        $progress->start();

        Connection::getTargetDB()->beginTransaction();

        $insertHostsSQL  = "INSERT INTO company_person (uuid, priority, name, text, photo, is_active) VALUES (:uuid, :priority, :name, :text, :photo, :is_active)";
        $insertHostsStmt = Connection::getTargetDB()->prepare($insertHostsSQL);

        foreach ($hosts as $host) {
            try {
                $host = $this->prepareHostObject($host);
                $insertHostsStmt->execute($host);
            } catch (\Exception $e) {
                Connection::getTargetDB()->rollBack();
                $logger->error('Error importing hosts: ' . $e->getMessage() . PHP_EOL . 'Trace: ' . $e->getTraceAsString(), array('content' => 'hosts'));
                $output->writeln(PHP_EOL . '<error>Error importing hosts: ' . $e->getMessage() . '</error>');
                exit();
            }
            $progress->advance();
        }
        Connection::getTargetDB()->commit();

        $progress->setMessage('Finished hosts import');
        $progress->finish();
        $output->writeln(PHP_EOL . '<info>Finished hosts import</info>');
        $logger->info('Finished hosts import, exit', array('content' => 'hosts'));
    }

    protected function prepareHostObject($item)
    {
        $config = HTMLPurifier_Config::createDefault();
        $config->set('HTML.Allowed', '');
        $removeHTML = new HTMLPurifier($config);

        $newObject              = array();
        $newObject['name']      = $item['name'];
        $newObject['uuid']      = str_replace('-', '', Uuid::uuid4()->toString());
        $newObject['priority']  = 99;
        $newObject['is_active'] = 1;
        $newObject['text']      = $removeHTML->purify($item['text']);
        if (!is_null($item['photo'])) {
            $newObject['photo'] = 'article_media/' . $item['photo'];
        } else {
            $newObject['photo'] = '';
        }

        return $newObject;
    }
}
