<?php

namespace Import360\Commands;

use Cocur\Slugify\Slugify;
use Import360\Db\Connection;
use Import360\Log\Logger;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class TagsImportCommand extends Command
{
    protected function configure()
    {
        $this->setName('import:tags')
             ->setDescription('Start tags import')
             ->addOption('fetch_limit', null, InputOption::VALUE_OPTIONAL, 'How many objects program will fetch from DB', 1000);
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $logger  = (new Logger('tags'))->getLogger();
        $slugify = new Slugify();

        $logger->info('Starting tags import', array('content' => 'tags'));
        $output->writeln(PHP_EOL . '<info>Starting tags import</info>');
        $countObjects = Connection::countSourceObjects('tags');
        $logger->info('Found ' . $countObjects . ' tags', array('content' => 'tags'));
        $limit      = $input->getOption('fetch_limit');
        $iterations = intval(ceil($countObjects / $limit));

        $progress = new ProgressBar($output, $countObjects);
        $progress->setMessage('Importing tags');
        $progress->start();

        while ($iterations) {
            $offset = intval(ceil($countObjects / $limit) - $iterations--) * $limit;

            $query = "SELECT id AS old_id, title AS name FROM [dbo].[tbl_tags] ORDER BY id OFFSET " . $offset . " ROWS FETCH NEXT " . $limit . " ROWS ONLY";
            $tags  = Connection::getSourceDB()->query($query)->fetchAll(\PDO::FETCH_ASSOC);

            Connection::getTargetDB()->beginTransaction();

            $insertTagsSQL  = 'INSERT INTO tags_tag (name, slug, is_main, show_in_videolist1, show_in_videolist2, old_id) VALUES (:name, :slug, 0, 0, 0, :old_id)';
            $insertTagsStmt = Connection::getTargetDB()->prepare($insertTagsSQL);

            foreach ($tags as $tag) {
                try {
                    $tag['slug'] = $slugify->slugify($tag['name']);
                    $insertTagsStmt->execute($tag);
                } catch (\Exception $e) {
                    Connection::getTargetDB()->rollBack();
                    $logger->error('Error importing tags: ' . $e->getMessage() . PHP_EOL . 'Trace: ' . $e->getTraceAsString(), array('content' => 'tags'));
                    $output->writeln(PHP_EOL . '<error>Error importing tags: ' . $e->getMessage() . '</error>');
                    exit();
                }
            }
            Connection::getTargetDB()->commit();
            $progress->advance(count($tags));
        }
        $progress->setMessage('Finished tags import');
        $progress->finish();
        $output->writeln(PHP_EOL . '<info>Finished tags import</info>');
        $logger->info('Finished tags import, exit', array('content' => 'tags'));
    }
}
