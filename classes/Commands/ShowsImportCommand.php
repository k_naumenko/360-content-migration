<?php

namespace Import360\Commands;

use HTMLPurifier;
use HTMLPurifier_Config;
use Import360\Db\Connection;
use Import360\Log\Logger;
use Ramsey\Uuid\Uuid;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class ShowsImportCommand extends Command
{
    protected function configure()
    {
        $this->setName('import:shows')
             ->setDescription('Start shows import')
             ->addOption('fetch_limit', null, InputOption::VALUE_OPTIONAL, 'How many objects program will fetch from DB', 1000);
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $logger = (new Logger('shows'))->getLogger();
        $logger->info('Starting shows import', array('content' => 'shows'));
        $output->writeln(PHP_EOL . '<info>Starting shows import</info>');

        $getShowsSQL = "SELECT
                          show.title,
                          show.age_limit,
                          show.description    AS text,
                          show.publish        AS is_active,
                          show.slug,
                          show.anons          AS lead,
                          show.timing         AS duration,
                          show.date_publish   AS pub_date,
                          show.date_insert    AS create_date,
                          show.date_last_edit AS update_date,
                          (SELECT TOP 1 i.url
                           FROM tbl_images AS i
                             JOIN tbl_images_links il ON il.id_image = i.id
                           WHERE il.obj_id = show.id AND il.main_image = 1 AND il.obj_type = 'Show')
                                              AS main_image
                        FROM [dbo].[tbl_shows] AS SHOW
                        ORDER BY id";
        $shows       = Connection::getSourceDB()->query($getShowsSQL)->fetchAll(\PDO::FETCH_ASSOC);

        $progress = new ProgressBar($output, count($shows));
        $progress->setMessage('Importing shows');
        $progress->start();

        Connection::getTargetDB()->beginTransaction();

        $insertShowsSQL  = "INSERT INTO
                              tv_program
                              (uuid, title, slug, lead, text, pub_date,create_date,update_date,is_active,priority,main_image,header_crop,age_limit,duration,mon,tue,wed,thr,fri,sat,sun,show_in_slider,list_image_height,show_in_coming_soon,show_in_promo,comments_count,is_comments_moderated)
                            VALUES
                              (:uuid, :title, :slug, :lead, :text, :pub_date,:create_date,:update_date,:is_active, 99,:main_image, '',:age_limit,:duration, 0, 0, 0, 0, 0, 0, 0, 0, 163, 0, 0, 0, 0)";
        $insertShowsStmt = Connection::getTargetDB()->prepare($insertShowsSQL);

        foreach ($shows as $show) {
            try {
                $show = $this->prepareShowsObject($show);
                $insertShowsStmt->execute($show);
            } catch (\Exception $e) {
                Connection::getTargetDB()->rollBack();
                $logger->error('Error importing shows: ' . $e->getMessage() . PHP_EOL . 'Trace: ' . $e->getTraceAsString(), array('content' => 'shows'));
                $output->writeln(PHP_EOL . '<error>Error importing shows: ' . $e->getMessage() . '</error>');
                exit();
            }
            $progress->advance();
        }
        Connection::getTargetDB()->commit();

        $progress->setMessage('Finished shows import');
        $progress->finish();
        $output->writeln(PHP_EOL . '<info>Finished shows import</info>');
        $logger->info('Finished shows import, exit', array('content' => 'shows'));
    }

    protected function prepareShowsObject($show)
    {
        $config = HTMLPurifier_Config::createDefault();
        $config->set('HTML.Allowed', '');
        $removeHTML = new HTMLPurifier($config);

        $show['uuid'] = str_replace('-', '', Uuid::uuid4()->toString());
        $show['text'] = $removeHTML->purify($show['text']);
        $show['lead'] = $removeHTML->purify($show['lead']);
        if (!is_null($show['main_image'])) {
            $show['main_image'] = 'article_media/' . $show['main_image'];
        }
        $show['age_limit'] = intval($show['age_limit']);
        $show['duration']  = $this->durationConverter($show['duration']);

        return $show;
    }

    protected function durationConverter($duration)
    {
        switch ($duration) {
            case "1 минута":
                $duration = 1;
                break;
            case "1 час":
                $duration = 60;
                break;
            case "1,5 часа":
                $duration = 90;
                break;
            case "10 минут":
                $duration = 10;
                break;
            case "16 минут":
                $duration = 16;
                break;
            case "16 минут, итоговый выпуск - 26 минут":
                $duration = 16;
                break;
            case "2 часа":
                $duration = 120;
                break;
            case "20 минут":
                $duration = 20;
                break;
            case "26 минут":
                $duration = 26;
                break;
            case "30 минут":
                $duration = 30;
                break;
            case "44 минуты":
                $duration = 44;
                break;
            case "50 мин":
            case "50 минут":
                $duration = 50;
                break;
            case "60 минут":
                $duration = 60;
                break;
            case "90 минут":
                $duration = 90;
                break;
            case null:
                $duration = null;
                break;
        }
        return $duration;
    }
}
