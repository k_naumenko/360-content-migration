<?php

namespace Import360\Commands;

use HTMLPurifier;
use HTMLPurifier_Config;
use Import360\Db\Connection;
use Import360\Log\Logger;
use Ramsey\Uuid\Uuid;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use voku\helper\HtmlDomParser;

class EpisodesImportCommand extends Command
{
    protected function configure()
    {
        $this->setName('import:episodes')
             ->setDescription('Start episodes import')
             ->addOption('fetch_limit', null, InputOption::VALUE_OPTIONAL, 'How many objects program will fetch from DB', 1000);
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $logger = (new Logger('episodes'))->getLogger();

        $logger->info('Starting episodes import', array('content' => 'episodes'));
        $output->writeln(PHP_EOL . '<info>Starting episodes import</info>');

        $countObjects = Connection::countSourceObjects('episodes');
        $logger->info('Found ' . $countObjects . ' episodes', array('content' => 'episodes'));
        $limit      = $input->getOption('fetch_limit');
        $iterations = intval(ceil($countObjects / $limit));

        $progress = new ProgressBar($output, $iterations * $limit);
        $progress->setMessage('Importing episodes');
        $progress->start();

        while ($iterations) {
            $offset         = intval(ceil($countObjects / $limit) - $iterations--) * $limit;
            $getEpisodesSQL = "SELECT
                                  e.id,
                                  e.title,
                                  e.slug,
                                  e.description    AS text,
                                  e.anons          AS lead,
                                  e.date_insert    AS create_date,
                                  e.date_last_edit AS update_date,
                                  e.date_publish   AS pub_date,
                                  e.publish        AS is_active,
                                  e.age_limit,
                                  e.show_id        AS program_id,
                                  (SELECT TOP 1 i.url
                                   FROM [dbo].[tbl_images] AS i
                                     JOIN [dbo].[tbl_images_links] il ON il.id_image = i.id
                                   WHERE il.obj_id = e.id AND il.main_image = 1 AND il.obj_type = 'Episode'
                                  )                AS main_image,
                                  (SELECT TOP 1 v.outsource_code
                                   FROM [dbo].[tbl_videos] AS v
                                     JOIN [dbo].[tbl_videos_links] vl ON vl.id_video = v.id
                                   WHERE vl.obj_id = e.id
                                  )                AS outsource_code,
                                  (SELECT TOP 1 a.date_insert
                                   FROM [dbo].[tbl_videos] AS a
                                     JOIN [dbo].[tbl_videos_links] b ON b.id_video = a.id
                                   WHERE b.obj_id = e.id
                                  )                AS video_date_insert
                                FROM [dbo].[tbl_episodes] AS e
                                ORDER BY e.id
                                  OFFSET " . $offset . " ROWS
                                  FETCH NEXT " . $limit . " ROWS ONLY";

            $episodes = Connection::getSourceDB()->query($getEpisodesSQL)->fetchAll(\PDO::FETCH_ASSOC);

            Connection::getTargetDB()->beginTransaction();

            $insertEpisodeSQL  = "INSERT INTO
                                    tv_serie
                                    (uuid, title, slug, lead, pub_date,create_date,update_date,is_active,main_image,age_limit,text,show_in_slider,show_in_coming_soon,show_in_promo,comments_count,is_comments_moderated, program_id, video_id)
                                  VALUES
                                    (:uuid, :title, :slug, :lead, :pub_date,:create_date,:update_date,:is_active,:main_image,:age_limit,:text,0,0,0,0,0, :program_id, :video_id)";
            $insertEpisodeStmt = Connection::getTargetDB()->prepare($insertEpisodeSQL);


            foreach ($episodes as $episode) {
                $id      = $episode['id'];
                $episode = $this->prepareEpisodesObject($episode);
                try {
                    if (array_key_exists('video_src', $episode)) {
                        $this->importRelatedVideo($episode, $insertEpisodeStmt);
                    } else {
                        $episode['video_id'] = null;
                        unset($episode['video_date_insert']);
                        $insertEpisodeStmt->execute($episode);
                    }

                } catch (\Exception $e) {
                    Connection::getTargetDB()->rollBack();
                    $logger->error('Error adding episodes object with id ' . $id . ' Error: ' . $e->getMessage() . PHP_EOL . 'Trace: ' . $e->getTraceAsString(), array('content' => 'episodes'));
                    $output->writeln(PHP_EOL . '<error>Error importing episodes: ' . $e->getMessage() . '</error>');
                    exit();
                }
            }
            Connection::getTargetDB()->commit();
            $progress->advance(count($episodes));
        }
        $progress->setMessage('Finished episodes import');
        $progress->finish();
        $output->writeln(PHP_EOL . '<info>Finished episodes import</info>');
        $logger->info('Finished episodes import, exit', array('content' => 'episodes'));
    }

    protected function prepareEpisodesObject($episode)
    {
        $config = HTMLPurifier_Config::createDefault();
        $config->set('HTML.Allowed', '');
        $removeHTML = new HTMLPurifier($config);

        $convertedEpisode                = array();
        $convertedEpisode['pub_date']    = $episode['pub_date'];
        $convertedEpisode['create_date'] = $episode['create_date'];
        $convertedEpisode['update_date'] = $episode['update_date'];
        $convertedEpisode['is_active']   = $episode['is_active'];
        $convertedEpisode['age_limit']   = intval($episode['age_limit']);
        $convertedEpisode['slug']        = $episode['slug'];
        $convertedEpisode['title']    = $episode['title'];

        $convertedEpisode['uuid'] = str_replace('-', '', Uuid::uuid4()->toString());
        if (!is_null($episode['text'])) {
            $convertedEpisode['text'] = $removeHTML->purify($episode['text']);
        } else {
            $convertedEpisode['text'] = '';
        }
        if (!is_null($episode['lead'])) {
            $convertedEpisode['lead'] = $removeHTML->purify($episode['lead']);
        } else {
            $convertedEpisode['lead'] = '';
        }
        $convertedEpisode['age_limit'] = intval($episode['age_limit']);
        if (!is_null($episode['program_id'])) {
            $convertedEpisode['program_id'] = $this->convertShowId($episode['program_id']);
        } else {
            $convertedEpisode['program_id'] = null;
        }
        if (!is_null($episode['main_image'])) {
            $convertedEpisode['main_image'] = 'article_media/' . $episode['main_image'];
        } else {
            $convertedEpisode['main_image'] = null;
        }

        if (!is_null($episode['outsource_code'])) {
            $html = HtmlDomParser::str_get_html($episode['outsource_code']);
            $e    = $html->find('iframe', 0);
            if (is_object($e) && $e->src) {
                $convertedEpisode['video_src']         = $e->src;
                $convertedEpisode['video_date_insert'] = $episode['video_date_insert'];
            }
        }
        return $convertedEpisode;
    }

    protected function convertShowId($id)
    {
        $showsIds = json_decode(file_get_contents(APP_PATH . '/shows/converted_id.json'), true);

        if (!array_key_exists($id, $showsIds)) {
            $id = null;
        } else {
            $id = $showsIds[$id];
        }
        return $id;
    }

    protected function importRelatedVideo($item, \PDOStatement $insertEpisodeStmt)
    {
        $insertVideoStmt     = Connection::getTargetDB()
                                         ->prepare("INSERT INTO video_video (uuid, title, url, show_in_recommend, show_on_main, show_on_main_slave) VALUES (:uuid, :title, :url, 0,0,0)");
        $insertVideoItemStmt = Connection::getTargetDB()
                                         ->prepare("INSERT INTO video_videoitem (pub_date, title, item_id, item_type_id, show_in_recommend, show_on_main, show_on_main_slave) VALUES (:pubdate, :title, :item_id, 27, 0,0,0)");

        $videoObject = [
            'uuid'  => str_replace('-', '', Uuid::uuid4()->toString()),
            'title' => $item['title'],
            'url'   => $item['video_src']
        ];
        $insertVideoStmt->execute($videoObject);
        $item['video_id'] = Connection::getTargetDB()->lastInsertId();
        $videoDateInsert  = $item['video_date_insert'];
        unset($item['video_date_insert']);
        unset($item['video_src']);
        $insertEpisodeStmt->execute($item);
        $objectId = Connection::getTargetDB()->lastInsertId();
        $insertVideoItemStmt->execute([
            ':pubdate' => $videoDateInsert,
            ':title'   => $item['title'],
            ':item_id' => $objectId
        ]);
    }
}
