<?php

namespace Import360\Commands;

use HTMLPurifier;
use HTMLPurifier_Config;
use Import360\Db\Connection;
use Import360\Log\Logger;
use Ramsey\Uuid\Uuid;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use voku\helper\HtmlDomParser;

class NewsImportCommand extends Command
{
    protected function configure()
    {
        $this->setName('import:news')
             ->setDescription('Start news import')
             ->addOption('fetch_limit', null, InputOption::VALUE_OPTIONAL, 'How many objects program will fetch from DB', 1000);
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $logger = (new Logger('news'))->getLogger();

        $logger->info('Starting news import', array('content' => 'news'));
        $output->writeln(PHP_EOL . '<info>Starting news import</info>');

        $countObjects = Connection::countSourceObjects('news');
        $logger->info('Found ' . $countObjects . ' news', array('content' => 'news'));
        $limit      = $input->getOption('fetch_limit');
        $iterations = intval(ceil($countObjects / $limit));

        $progress = new ProgressBar($output, $countObjects);
        $progress->setMessage('Importing news');
        $progress->start();

        while ($iterations) {
            $offset  = intval(ceil($countObjects / $limit) - $iterations--) * $limit;
            $sqlNews = "SELECT t.id, t.title, t.title AS title_main, t.slug, t.anons AS lead, t.text, t.date_publish AS pub_date, t.date_insert AS create_date, t.date_last_edit AS update_date, t.id_theme AS category_id, t.publish AS is_active,
                          (SELECT TOP 1 url
                            FROM [dbo].[tbl_images] AS i
                            INNER JOIN [dbo].[tbl_images_links] l ON i.id = l.id_image
                            WHERE obj_type = 'news' AND l.obj_id = t .id
                            ORDER BY main_image DESC)
                          AS image_url,
                          (SELECT TOP 1 outsource_code
                            FROM [dbo].[tbl_videos] AS v
                            INNER JOIN [dbo].[tbl_videos_links] vl ON v.id = vl.id_video
                            WHERE vl.obj_type = 'news' AND vl.obj_id = t.id)
                          AS video_code,
					      (SELECT TOP 1 a.date_insert AS video_date_insert
                            FROM [dbo].[tbl_videos] AS a
                            INNER JOIN [dbo].[tbl_videos_links] b ON a.id = b.id_video
                            WHERE b.obj_type = 'news' AND b.obj_id = t.id)
                          AS video_date_insert
                        FROM dbo.tbl_news AS t
                        WHERE id_theme IS NOT NULL
                        ORDER BY id OFFSET " . $offset . " ROWS FETCH NEXT " . $limit . " ROWS ONLY";

            $result = Connection::getSourceDB()->query($sqlNews)->fetchAll(\PDO::FETCH_ASSOC);

            Connection::getTargetDB()->beginTransaction();

            $insertNewsSQL  = "INSERT INTO
                                  news_article
                                  (title, title_main, slug, lead, text, pub_date, create_date, update_date, category_id, uuid, main_image, is_active, show_in_slider, story_block, show_video_on_top, show_image, show_in_coming_soon, show_in_promo, comments_count, is_comments_moderated, video_id)
                                VALUES
                                  (:title, :title_main, :slug, :lead, :text, :pub_date, :create_date, :update_date, :category_id, :uuid, :main_image, :is_active, 0, 0, 0, :show_image, 0, 0, 0, 0, :video_id)";
            $insertNewsStmt = Connection::getTargetDB()->prepare($insertNewsSQL);


            foreach ($result as $item) {
                $id = $item['id'];
                $item = $this->prepareNewsObject($item);
                try {
                    if (array_key_exists('video_src', $item)) {
                        $objectId = $this->importRelatedVideo($item, $insertNewsStmt);
                    } else {
                        $item['video_id'] = null;
                        $insertNewsStmt->execute($item);
                        $objectId = Connection::getTargetDB()->lastInsertId();
                    }
                    $this->updateTagsRelations($objectId);

                } catch (\Exception $e) {
                    Connection::getTargetDB()->rollBack();
                    $logger->error('Error adding news object with id ' . $id . ' Error: ' . $e->getMessage() . PHP_EOL . 'Trace: ' . $e->getTraceAsString(), array('content' => 'news'));
                    $output->writeln(PHP_EOL . '<error>Error importing news: ' . $e->getMessage() . '</error>');
                    exit();
                }
            }
            Connection::getTargetDB()->commit();
            $progress->advance(count($result));
        }
        $progress->setMessage('Finished news import');
        $progress->finish();
        $output->writeln(PHP_EOL . 'Finished news import');
        $logger->info('Finished news import, exit', array('content' => 'news'));
    }

    protected function prepareNewsObject($item)
    {
        $config = HTMLPurifier_Config::createDefault();
        $config->set('HTML.Allowed', '');
        $removeHTML = new HTMLPurifier($config);

        $newObject                = array();
        $newObject['title']       = $item['title'];
        $newObject['title_main']  = $item['title'];
        $newObject['slug']        = $item['slug'];
        $newObject['lead']        = $removeHTML->purify($item['lead']);
        $newObject['text']        = isset($item['text']) ? str_replace('/binfiles/images/', '/media/article_media/', $item['text']) : '';
        $newObject['pub_date']    = $item['pub_date'];
        $newObject['create_date'] = $item['create_date'];
        $newObject['update_date'] = $item['update_date'];
        $newObject['category_id'] = $this->convertCategory($item['category_id']);

        $newObject['uuid'] = str_replace('-', '', Uuid::uuid4()->toString());
        if (!is_null($item['image_url'])) {
            $newObject['main_image'] = 'article_media/' . $item['image_url'];
            $newObject['show_image'] = 1;
        } else {
            $newObject['main_image'] = null;
            $newObject['show_image'] = 0;
        }
        $newObject['is_active'] = $item['is_active'];
        if (!is_null($item['video_code'])) {
            $html = HtmlDomParser::str_get_html($item['video_code']);
            $e    = $html->find('iframe', 0);
            if (is_object($e) && $e->src) {
                $newObject['video_src']         = $e->src;
                $newObject['video_date_insert'] = $item['video_date_insert'];
            }
        }

        return $newObject;
    }

    protected function convertCategory($id)
    {
        $categoriesTable = json_decode(file_get_contents(APP_PATH . '/news_categories/converted_id.json'), true);

        if (!array_key_exists($id, $categoriesTable)) {
            $id = 99;
        } else {
            $id = $categoriesTable[$id];
        }
        return $id;
    }

    protected function importRelatedVideo($item, \PDOStatement $insertNewsStmt)
    {
        $insertVideoStmt     = Connection::getTargetDB()
                                         ->prepare('INSERT INTO video_video (uuid, title, url, show_in_recommend, show_on_main, show_on_main_slave) VALUES (:uuid, :title, :url, 0,0,0)');
        $insertVideoItemStmt = Connection::getTargetDB()
                                         ->prepare('INSERT INTO video_videoitem (pub_date, title, item_id, item_type_id, show_in_recommend, show_on_main, show_on_main_slave) VALUES (:pubdate, :title, :item_id, 13, 0,0,0)');
        $videoObject         = [
            'uuid'  => str_replace('-', '', Uuid::uuid4()->toString()),
            'title' => $item['title'],
            'url'   => $item['video_src']
        ];
        $insertVideoStmt->execute($videoObject);
        $item['video_id'] = Connection::getTargetDB()->lastInsertId();
        unset($item['video_src']);
        $videoDateInsert = $item['video_date_insert'];
        unset($item['video_date_insert']);
        $insertNewsStmt->execute($item);
        $objectId = Connection::getTargetDB()->lastInsertId();
        $insertVideoItemStmt->execute([
            ':pubdate' => $videoDateInsert,
            ':title'   => $item['title'],
            ':item_id' => $objectId
        ]);
        return $objectId;
    }

    protected function updateTagsRelations($id)
    {
        $tags = Connection::getSourceDB()
                          ->query("SELECT id_tag AS id FROM tbl_tags_links WHERE id_object = '" . $id . "' AND obj_type = 'news'")
                          ->fetchAll(\PDO::FETCH_ASSOC);

        foreach ($tags as $tag) {
            $newId = Connection::getTargetDB()
                               ->query("SELECT id FROM tags_tag WHERE old_id = '" . $tag['id'] . "'")
                               ->fetchColumn();

            $insertTagSQL  = "INSERT INTO tags_tagged (object_id, content_type_id, tag_id, is_active, show_on_tag_page) VALUES (:object_id, :content_type_id, :tag_id, 1, 1)";
            $insertTagStmt = Connection::getTargetDB()->prepare($insertTagSQL);
            $insertTagStmt->execute([
                ':object_id'       => $id,
                ':content_type_id' => '13',
                ':tag_id'          => $newId
            ]);
        }
    }
}
