<?php

namespace Import360\Commands;

use Import360\Db\Connection;
use Import360\Log\Logger;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class PollsImportCommand extends Command
{
    protected function configure()
    {
        $this->setName('import:polls')
             ->setDescription('Start polls import')
             ->addOption('fetch_limit', null, InputOption::VALUE_OPTIONAL, 'How many objects program will fetch from DB', 1000);
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $logger = (new Logger('polls'))->getLogger();
        $logger->info('Starting polls import', array('content' => 'polls'));
        $output->writeln(PHP_EOL . '<info>Starting polls import</info>');

        $getPollsSQL = "SELECT id, title, publish AS is_active, publish AS is_opened FROM [dbo].[tbl_polls] ORDER BY id";
        $polls       = Connection::getSourceDB()->query($getPollsSQL)->fetchAll(\PDO::FETCH_ASSOC);

        $progress = new ProgressBar($output, count($polls));
        $progress->setMessage('Importing polls');
        $progress->start();

        Connection::getTargetDB()->beginTransaction();

        $insertPollsSQL  = 'INSERT INTO polls_poll (title, is_active, is_opened) VALUES (:title, :is_active, :is_opened)';
        $insertPollsStmt = Connection::getTargetDB()->prepare($insertPollsSQL);

        foreach ($polls as $poll) {
            try {
                $selectPollOptionsSQL = "SELECT answer AS title, voters AS votes FROM [dbo].[tbl_polls_answers] WHERE id_poll = " . $poll['id'];
                $pollOptions          = Connection::getSourceDB()
                                                  ->query($selectPollOptionsSQL)
                                                  ->fetchAll(\PDO::FETCH_ASSOC);
                unset($poll['id']);
                $insertPollsStmt->execute($poll);
                $pollId                = Connection::getTargetDB()->lastInsertId();
                $insertPollOptionsSQL  = "INSERT INTO polls_polloption (is_active, title,  poll_id, votes, priority) VALUES (1, :title,  " . $pollId . ", :votes, 0)";
                $insertPollOptionsStmt = Connection::getTargetDB()->prepare($insertPollOptionsSQL);
                foreach ($pollOptions as $pollOption) {
                    $insertPollOptionsStmt->execute($pollOption);
                }
            } catch (\Exception $e) {
                Connection::getTargetDB()->rollBack();
                $logger->error('Error importing polls: ' . $e->getMessage() . PHP_EOL . 'Trace: ' . $e->getTraceAsString(), array('content' => 'polls'));
                $output->writeln(PHP_EOL . '<error>Error importing polls: ' . $e->getMessage() . PHP_EOL . 'Trace: ' . $e->getTraceAsString() . '</error>');
                exit();
            }
            $progress->advance();
        }
        Connection::getTargetDB()->commit();

        $progress->setMessage('Finished polls import');
        $progress->finish();
        $output->writeln(PHP_EOL . '<info>Finished polls import</info>');
        $logger->info('Finished polls import, exit', array('content' => 'polls'));
    }
}
