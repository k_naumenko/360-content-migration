<?php

namespace Import360\Commands;

use Import360\Db\Connection;
use Import360\Log\Logger;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class WipeDatabaseCommand extends Command
{

    protected function configure()
    {
        $this->setName('import:wipe');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $logger  = (new Logger('full'))->getLogger();
        $output->writeln(PHP_EOL . '<info>Starting DB wipe</info>');
        try {
            Connection::getTargetDB()->beginTransaction();
            Connection::getTargetDB()->exec('DELETE FROM tags_tagged');
            Connection::getTargetDB()->exec('ALTER TABLE tags_tagged AUTO_INCREMENT=1');
            Connection::getTargetDB()->exec('DELETE FROM news_article');
            Connection::getTargetDB()->exec('ALTER TABLE news_article AUTO_INCREMENT=1');
            Connection::getTargetDB()->exec('DELETE FROM tv_tvslot');
            Connection::getTargetDB()->exec('ALTER TABLE tv_tvslot AUTO_INCREMENT=1');
            Connection::getTargetDB()->exec('DELETE FROM tv_serie');
            Connection::getTargetDB()->exec('ALTER TABLE tv_serie AUTO_INCREMENT=1');
            Connection::getTargetDB()->exec('DELETE FROM video_video ');
            Connection::getTargetDB()->exec('ALTER TABLE video_video AUTO_INCREMENT=1');
            Connection::getTargetDB()->exec('DELETE FROM video_videoitem');
            Connection::getTargetDB()->exec('ALTER TABLE video_videoitem AUTO_INCREMENT=1');
            Connection::getTargetDB()->exec('DELETE FROM tv_program_speakers');
            Connection::getTargetDB()->exec('ALTER TABLE tv_program_speakers AUTO_INCREMENT=1');
            Connection::getTargetDB()->exec('DELETE FROM tv_program');
            Connection::getTargetDB()->exec('ALTER TABLE tv_program AUTO_INCREMENT=1');
            Connection::getTargetDB()->exec('DELETE FROM company_person');
            Connection::getTargetDB()->exec('ALTER TABLE company_person AUTO_INCREMENT=1');
            Connection::getTargetDB()->exec('DELETE FROM tags_tag');
            Connection::getTargetDB()->exec('ALTER TABLE tags_tag AUTO_INCREMENT=1');
            Connection::getTargetDB()->exec('DELETE FROM polls_polloption');
            Connection::getTargetDB()->exec('ALTER TABLE polls_polloption AUTO_INCREMENT=1');
            Connection::getTargetDB()->exec('DELETE FROM polls_poll');
            Connection::getTargetDB()->exec('ALTER TABLE polls_poll AUTO_INCREMENT=1');
            Connection::getTargetDB()->exec('DELETE FROM company_satelliteoperator');
            Connection::getTargetDB()->exec('ALTER TABLE company_satelliteoperator AUTO_INCREMENT=1');
            Connection::getTargetDB()->commit();
        } catch (\Exception $e) {
            Connection::getTargetDB()->rollBack();
            $logger->error('Error wiping DB: ' . $e->getMessage(). PHP_EOL . 'Trace: ' . $e->getTraceAsString(), array('content' => 'wipe'));
            $output->writeln(PHP_EOL . '<error>Error wiping DB: ' . $e->getMessage() . '</error>');
            exit();
        }
        $output->writeln(PHP_EOL . '<info>Finished DB wipe</info>');
    }
}
