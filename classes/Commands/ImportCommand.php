<?php

namespace Import360\Commands;

use Import360\Log\Logger;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class ImportCommand extends Command
{

    protected function configure()
    {
        $this->setName('import')
             ->setDescription('Start DB import')
             ->addOption('fetch_limit', null, InputOption::VALUE_OPTIONAL, 'How many objects program will fetch from DB', 1000);

    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $logger = (new Logger('full'))->getLogger();
        $logger->info('Starting full import', array('content' => 'full'));
        $output->writeln(PHP_EOL . '<info>Starting full import</info>');

        $count = 0;
        foreach ($this->getApplication()->all() as $command) {
            if (strpos($command->getName(), 'import:') !== false) {
                $count++;
            }
        }

        $progress = new ProgressBar($output, $count);
        $progress->setMessage('Starting DB import');
        $progress->start();

        foreach ($this->getApplication()->all() as $command) {
            if (strpos($command->getName(), 'import:') !== false) {
                $command->run($input, $output);
                $progress->advance();
            }
        }

        $progress->setMessage('ImportFinished');
        $progress->finish();
    }
}
