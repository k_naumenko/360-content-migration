<?php

namespace Import360\Commands;

use HTMLPurifier;
use HTMLPurifier_Config;
use Import360\Db\Connection;
use Import360\Log\Logger;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class OperatorsImportCommand extends Command
{
    protected function configure()
    {
        $this->setName('import:operators')
             ->setDescription('Start operators import')
             ->addOption('fetch_limit', null, InputOption::VALUE_OPTIONAL, 'How many objects program will fetch from DB', 1000);
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $logger = (new Logger('operators'))->getLogger();
        $logger->info('Starting operators import', array('content' => 'operators'));
        $output->writeln(PHP_EOL . '<info>Starting operators import</info>');

        $getOperatorsSQL = "SELECT
                              [title] AS name,
                              [text] AS description,
                              [publish] AS is_active,
                              [hd] AS is_hd,
                              [order] AS priority
                            FROM [dbo].[tbl_operators]";
        $operators       = Connection::getSourceDB()->query($getOperatorsSQL)->fetchAll(\PDO::FETCH_ASSOC);

        $progress = new ProgressBar($output, count($operators));
        $progress->setMessage('Importing operators');
        $progress->start();

        Connection::getTargetDB()->beginTransaction();

        $insertOperatorsSQL  = "INSERT INTO
                                  company_satelliteoperator
                                  (name, description, is_active, is_hd, priority)
                                VALUES
                                  (:name, :description, :is_active, :is_hd, :priority)";
        $insertOperatorsStmt = Connection::getTargetDB()->prepare($insertOperatorsSQL);

        foreach ($operators as $operator) {
            try {
                $operator = $this->prepareOperatorObject($operator);
                $insertOperatorsStmt->execute($operator);
            } catch (\Exception $e) {
                Connection::getTargetDB()->rollBack();
                $logger->error('Error importing operators: ' . $e->getMessage() . PHP_EOL . 'Trace: ' . $e->getTraceAsString(), array('content' => 'operators'));
                $output->writeln(PHP_EOL . '<error>Error importing operators: ' . $e->getMessage() . '</error>');
                exit();
            }
            $progress->advance();
        }
        Connection::getTargetDB()->commit();

        $progress->setMessage('Finished operators import');
        $progress->finish();
        $output->writeln(PHP_EOL . '<info>Finished operators import</info>');
        $logger->info('Finished operators import, exit', array('content' => 'operators'));
    }

    protected function prepareOperatorObject($operator)
    {
        $config = HTMLPurifier_Config::createDefault();
        $config->set('HTML.Allowed', '');
        $removeHTML = new HTMLPurifier($config);

        $operator['description'] = $removeHTML->purify($operator['description']);

        return $operator;
    }
}
