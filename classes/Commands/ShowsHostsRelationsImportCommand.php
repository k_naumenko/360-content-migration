<?php

namespace Import360\Commands;

use Import360\Db\Connection;
use Import360\Log\Logger;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class ShowsHostsRelationsImportCommand extends Command
{
    protected function configure()
    {
        $this->setName('import:shows-hosts')
             ->setDescription('Start shows and hosts relations import')
             ->addOption('fetch_limit', null, InputOption::VALUE_OPTIONAL, 'How many objects program will fetch from DB', 1000);
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $logger = (new Logger('shows-hosts'))->getLogger();
        $logger->info('Starting shows-hosts import', array('content' => 'shows-hosts'));
        $output->writeln(PHP_EOL . '<info>Starting shows-hosts import</info>');

        $getRelationsSQL = "SELECT
                              [show_id] AS program_id,
                              [host_id] AS person_id
                            FROM [dbo].[tbl_shows_link_hosts]";
        $relations       = Connection::getSourceDB()->query($getRelationsSQL)->fetchAll(\PDO::FETCH_ASSOC);

        $progress = new ProgressBar($output, count($relations));
        $progress->setMessage('Importing shows-hosts');
        $progress->start();

        Connection::getTargetDB()->beginTransaction();

        $insertRelationsSQL  = "INSERT INTO
                                  tv_program_speakers
                                  (program_id, person_id)
                                VALUES
                                  (:program_id, :person_id)";
        $insertRelationsStmt = Connection::getTargetDB()->prepare($insertRelationsSQL);

        foreach ($relations as $relation) {
            try {
                $relation = $this->convertRelationIds($relation);
                $insertRelationsStmt->execute($relation);
            } catch (\Exception $e) {
                Connection::getTargetDB()->rollBack();
                $logger->error('Error importing shows-hosts: ' . $e->getMessage() . PHP_EOL . 'Trace: ' . $e->getTraceAsString(), array('content' => 'shows-hosts'));
                $output->writeln(PHP_EOL . '<error>Error importing shows-hosts: ' . $e->getMessage() . '</error>');
                exit();
            }
            $progress->advance();
        }
        Connection::getTargetDB()->commit();

        $progress->setMessage('Finished shows-hosts import');
        $progress->finish();
        $output->writeln(PHP_EOL . '<info>Finished shows-hosts import</info>');
        $logger->info('Finished shows-hosts import, exit', array('content' => 'shows-hosts'));
    }

    protected function convertRelationIds($object)
    {
        $showIds  = json_decode(file_get_contents(APP_PATH . '/shows/converted_id.json'), true);
        $hostsIds = json_decode(file_get_contents(APP_PATH . '/hosts/converted_id.json'), true);

        $object['program_id'] = $showIds[$object['program_id']];
        $object['person_id']  = $hostsIds[$object['person_id']];
        return $object;
    }
}
