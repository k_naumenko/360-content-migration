<?php
namespace Import360\DB;

use PDOStatement;

class CustomPDOStatement extends PDOStatement
{
    public function execute($data = array())
    {
        parent::execute($data);
        return $this;
    }
}