<?php
namespace Import360\Db;

use Exception;
use PDO;

class Connection
{
    private static $targetDB = null;
    private static $sourceDB = null;
    private static $sourceTables = array(
        'tags'     => '[dbo].[tbl_tags]',
        'news'     => '[dbo].[tbl_news]',
        'episodes' => '[dbo].[tbl_episodes]',
    );
    private static $targetTables = array(
        'tags'     => 'tags_tag',
        'news'     => 'news_article',
        'episodes' => 'tv_serie',
    );

    final private function __construct()
    {
    }

    public static function getTargetTableName($contentType)
    {
        return self::$targetTables[$contentType];
    }

    public static function countSourceObjects($contentType)
    {
        return self::getSourceDB()
                   ->query("SELECT COUNT(*) AS count FROM " . self::getSourceTableName($contentType))
                   ->fetchColumn();
    }

    public static function getSourceDB()
    {
        if (self::$sourceDB === null) {
            $options  = array(
                PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
                PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC
            );
            $dsn      = "odbc:mssql";
            $user     = '';
            $password = '';
            try {
                self::$sourceDB = new PDO($dsn, $user, $password, $options);
            } catch (Exception $e) {
                echo 'Caught exception: ' . $e->getMessage() . PHP_EOL . $e->getTraceAsString();
            }
        }
        return self::$sourceDB;
    }

    public static function getSourceTableName($contentType)
    {
        return self::$sourceTables[$contentType];
    }

    public static function getTargetDB()
    {
        if (self::$targetDB === null) {
            $options  = array(
                PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
                PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
                PDO::ATTR_EMULATE_PREPARES   => true,
                PDO::ATTR_STATEMENT_CLASS    => array('Import360\Db\CustomPDOStatement')
            );
            $host = '';
            $dbname = '';
            $dsn      = "mysql:host=$host;dbname=$dbname;charset=utf8";
            $user     = '';
            $password = '';
            try {
                self::$targetDB = new PDO($dsn, $user, $password, $options);
            } catch (Exception $e) {
                echo 'Caught exception: ' . $e->getMessage() . PHP_EOL . $e->getTraceAsString();
            }
        }
        return self::$targetDB;
    }

    final private function __clone()
    {
    }
}
