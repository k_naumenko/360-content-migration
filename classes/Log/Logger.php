<?php

namespace Import360\Log;

use Monolog\Handler\StreamHandler;

class Logger
{
    private $logger;
    private $typePaths = array(
        'full'        => APP_PATH . '/logs/full.log',
        'tags'        => APP_PATH . '/logs/tags.log',
        'news'        => APP_PATH . '/logs/news.log',
        'polls'       => APP_PATH . '/logs/polls.log',
        'hosts'       => APP_PATH . '/logs/hosts.log',
        'operators'   => APP_PATH . '/logs/operators.log',
        'shows-hosts' => APP_PATH . '/logs/shows-hosts.log',
        'shows'       => APP_PATH . '/logs/shows.log',
        'episodes'    => APP_PATH . '/logs/episodes.log',
    );

    public function __construct($type)
    {
        $this->logger = new \Monolog\Logger('Progress Log');
        $this->logger->pushHandler(new StreamHandler($this->typePaths[$type], \Monolog\Logger::INFO));
        if ($type != 'full') {
            $this->logger->pushHandler(new StreamHandler($this->typePaths['full'], \Monolog\Logger::INFO));
        }
    }

    public function getLogger()
    {
        return $this->logger;
    }
}
